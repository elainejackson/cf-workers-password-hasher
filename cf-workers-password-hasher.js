const encoder = new TextEncoder();

export async function createPasswordHash(password, salt = null) {
  // Generate a random salt if not provided
  if (!salt) {
    salt = Array.from(crypto.getRandomValues(new Uint8Array(16)))
      .map(b => b.toString(16).padStart(2, '0'))
      .join('');
  }

  const cryptoKey = await crypto.subtle.importKey(
    'raw',
    encoder.encode(password),
    { name: 'PBKDF2' },
    false,
    ['deriveKey', 'deriveBits']
  );

  const hash = await crypto.subtle.deriveBits(
    {
      name: 'PBKDF2',
      salt: encoder.encode(salt),
      iterations: 100000,
      hash: 'SHA-256'
    },
    cryptoKey,
    256
  );

  // Convert the hash to a string and append the salt
  return Array.from(new Uint8Array(hash))
    .map(b => b.toString(16).padStart(2, '0'))
    .join('') + salt;
}

export async function verifyPasswordHash(password, hashedPasswordWithSalt) {
  // Extract the salt from the hashed password
  let salt = hashedPasswordWithSalt.slice(-32);

  // Hash the password provided by the user
  const hashedPassword = await createPasswordHash(password, salt);

  // Compare the hashed password with the stored hash
  return hashedPassword === hashedPasswordWithSalt;
}

export async function testHashingAndVerification() {
  const password = 'password';
  const hashedPassword = await createPasswordHash(password);

  console.log(`Original password: ${password}`);
  console.log(`Hashed password: ${hashedPassword}`);

  const verificationResult = await verifyPasswordHash(password, hashedPassword);
  console.log(`Verification result: ${verificationResult}`);
}
