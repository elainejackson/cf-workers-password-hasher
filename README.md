# CF Workers Password Hasher
A simple package to create and verify password hashes using the WebCrypto API available in Cloudflare Workers.

## Usage
First install the package `npm install https://gitlab.com/elainejackson/cf-workers-password-hasher`

```
import { createPasswordHash, verifyPasswordHash } from 'cf-workers-password-hasher/cf-workers-password-hasher';

createPasswordHash("Super Secret Password")
verifyPasswordHash("Super Secret Password", "hashedPasswordFromStorage") // true
verifyPasswordHash("I forgot my password", "hashedPasswordFromStorage") // false
```
